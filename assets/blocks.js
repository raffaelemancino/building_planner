var blocks = [
    { "id": 1, "name": "stone",  "texture": "stone" },
    { "id": 2, "name": "grass", "texture": "grass_side" },
    { "id": 3, "name": "dirt", "texture": "dirt" },
    { "id": 4, "name": "cobblestone", "texture": "cobblestone" },
    { "id": 20, "name": "glass", "texture": "glass" },
]
